package superadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import utility.Constant;

public class ArchiveLeadersOfClient {
	//Working fine

		public static void main(String[] args) throws InterruptedException {
			System.setProperty("webdriver.chrome.driver","/Users/janetrajani/eclipse-workspace/chromedriver");
			WebDriver driver = new ChromeDriver();
			//Login
			driver.get(Constant.URL+"/login");
			driver.manage().window().fullscreen();
			driver.findElement(By.name("email")).sendKeys(Constant.USERNAME);
			driver.findElement(By.name("password")).sendKeys(Constant.PASSWORD);
			//Hit submit button on login page
			driver.findElement(By.className("btn-lg")).click();
			//End Login
			
			Thread.sleep(5000);
			//Search client with a particular name
			driver.findElement(By.xpath("//*[@id='app']/div/div[3]/div[1]/div[1]/form/input")).sendKeys(Constant.CLIENTNAME);
			driver.findElement(By.className("search-btn")).click();
			
			Thread.sleep(3000);		
			//Hit the client name in your search result
			driver.findElement(By.partialLinkText(Constant.CLIENTNAME)).click();
			
			int i =0;
			//Archive multiple leaders under this client one by one
			for( i=1; i<30; i++) {
				
				Thread.sleep(3000);		
				//Click on the right side dots
				driver.findElement(By.cssSelector("ul.navbar-nav li.nav-item a#navbarDropdownMenuLink .fa-ellipsis-h.pull-right")).click();
				//Hit the archive link on popup
				driver.findElement(By.linkText("Archive")).click();
				//click on confirmation box
				driver.findElement(By.cssSelector(".custom-ui button.btn-primary")).click();
				//Click on Invite link
				//driver.findElement(By.cssSelector(".dropdown-menu.dropdown-menu-right.show .dropdown-item")).click();
		    }	
			driver.quit();
		}
	}
