package superadmin;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import utility.Constant;
//working fine
public class AddMultipleLPOneLeader {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","/Users/janetrajani/eclipse-workspace/chromedriver");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//Login
		driver.get(Constant.URL+"/login");
		driver.manage().window().fullscreen();
		driver.findElement(By.name("email")).sendKeys(Constant.USERNAME);
		driver.findElement(By.name("password")).sendKeys(Constant.PASSWORD);
		//Hit submit button on login page
		driver.findElement(By.className("btn-lg")).click();
		//End Login
		
		Thread.sleep(5000);
		//Search client with a keyword
		driver.findElement(By.xpath("//*[@id='app']/div/div[3]/div[1]/div[1]/form/input")).sendKeys(Constant.CLIENTNAME);
		driver.findElement(By.className("search-btn")).click();
		
		Thread.sleep(3000);		
		
		driver.findElement(By.partialLinkText(Constant.CLIENTNAME)).click();
		
		for(char alphabet = 'a'; alphabet <='z'; alphabet++ )
	    {
			Thread.sleep(3000);
			//hit existing leader
			driver.findElement(By.className("text-capitalize")).click();
			Thread.sleep(4000);	
			//Hit add new launch plan button
			//add launch plan details
			driver.findElement(By.className("btn-primary")).click();
			Thread.sleep(3000);	
			
			//Each time the value is set to blank
			String suffix = "";
			suffix += alphabet;
	        //add launch plan name
	        driver.findElement(By.name("plan_name")).sendKeys(Constant.LAUNCH_PLAN_NAME+suffix+suffix);
	        //scroll down on the page
	        js.executeScript("window.scrollBy(0,1000)");
	        //select module name
	        Select drpCountry = new Select(driver.findElement(By.name("launch_type")));
			drpCountry.selectByVisibleText(Constant.LAUNCH_PLAN_TYPE);
			
			//checkbox selection
			List<WebElement> moduleboxList = driver.findElements(By.name("module"));
			int moduleListCount = moduleboxList.size();
			
			Thread.sleep(3000);	
			int i =0;
			for( i=1; i<moduleListCount; i++) {
				String moduleValue = moduleboxList.get(i).getAttribute("value");
				Thread.sleep(2000);	
				//moduleboxList.get(i).click();
				moduleboxList.get(6).click();
				moduleboxList.get(1).click();

				break;
			}

			//End checkbox selection
		
			//hit save button
			Thread.sleep(4000);	
			driver.findElement(By.cssSelector(".box-footer button.btn-primary")).click();
			Thread.sleep(2000);	
			driver.navigate().back();
			
	    }	
		Thread.sleep(3000);	
		driver.quit();
	}

}
