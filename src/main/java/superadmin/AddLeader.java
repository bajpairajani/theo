package superadmin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import utility.Constant;
//working fine
public class AddLeader {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","/Users/janetrajani/eclipse-workspace/chromedriver");
		WebDriver driver = new ChromeDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//Login
		driver.get(Constant.URL+"/login");
		driver.manage().window().fullscreen();
		driver.findElement(By.name("email")).sendKeys(Constant.USERNAME);
		driver.findElement(By.name("password")).sendKeys(Constant.PASSWORD);
		//Hit submit button on login page
		driver.findElement(By.className("btn-lg")).click();
		//End Login
		
		Thread.sleep(5000);
		//Search client with a particular name
		driver.findElement(By.xpath("//*[@id='app']/div/div[3]/div[1]/div[1]/form/input")).sendKeys(Constant.CLIENTNAME);
		driver.findElement(By.className("search-btn")).click();
		
		Thread.sleep(3000);		
		
		driver.findElement(By.partialLinkText(Constant.CLIENTNAME)).click();
		//Initialize a variable to use it in email address to get multiple unique email addresses
		int i =31;
		for(char alphabet = 'a'; alphabet <='z'; alphabet++ )
	    {
			Thread.sleep(2000);
			String suffix = "";
			suffix += alphabet;
			//Hit add new leader button
			driver.findElement(By.xpath("//*[@id=\"leaderListHolder\"]/div[1]/div[3]/button")).click();
			driver.findElement(By.name("title")).sendKeys("Dr.");
			driver.findElement(By.name("first_name")).sendKeys(Constant.LEADER_FIRSTNAME);
			js.executeScript("window.scrollBy(0,1000)");
			driver.findElement(By.name("last_name")).sendKeys(Constant.LEADER_LASTNAME+suffix+suffix);
			driver.findElement(By.name("suffix")).sendKeys("Beta");
			driver.findElement(By.name("job_title")).sendKeys("Beta");
			driver.findElement(By.name("email")).sendKeys("janet.rajani+"+i+"@ithands.net");
			
			Select userType = new Select(driver.findElement(By.name("user_type")));
			userType.selectByValue("3");
			Thread.sleep(1000);
			//Save leader details
			driver.findElement(By.xpath("//*[@id=\"leaderInfoHolder\"]/div[1]/div/form/div/div/div[9]/button")).click();
			Thread.sleep(2000);
			//55 is the client ID based on this URL the system can directly go to leader list
			driver.get(Constant.URL+Constant.LEADERS_LIST_OF_CLIENT);
			
			//Increase the variable to get another unique email address
			i++;
	    }
		driver.quit();
	}

}
