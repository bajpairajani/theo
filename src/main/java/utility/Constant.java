package utility;

public class Constant {

		//Change website URL to stating or production
		public static final String URL = "https://beta.executivelaunchpad.net";
		
		//Super admin credentials
		public static final String USERNAME = "rajanitest@notworking.com";
		public static final String PASSWORD = "test@123";
		//End Super amdin credentials
		
		//set client's partial name
		public static final String CLIENTNAME = "UAT third phase";
		//This URL represents leader list under a particular client
		public static final String LEADERS_LIST_OF_CLIENT = "/leaders/55";
		
		public static final String LEADER_FIRSTNAME = "Uninvited Janet";
		public static final String LEADER_LASTNAME = "Test";
		
		//Launch plan details
		public static final String LAUNCH_PLAN_NAME = "Beta LP third phase";
		public static final String LAUNCH_PLAN_TYPE = "Alignment";
		public static final String LAUNCH_LEADER_NAME = "JanetUAT ClientAdmin";
	

}
