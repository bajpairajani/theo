package superadmin_crm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddCrmClient {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","/Users/janetrajani/eclipse-workspace/chromedriver");
				WebDriver driver = new ChromeDriver();
				driver.get("https://execlaunchpadstaging.ithands.net/login");
				driver.findElement(By.name("email")).sendKeys("janet.rajani@ithands.net");
				driver.findElement(By.name("password")).sendKeys("Rubi@123");
				//Hit submit button on login page
				driver.findElement(By.className("btn-lg")).click();
				Thread.sleep(3000);
				//Hit CRM tab
				driver.findElement(By.linkText("CRM")).click();
				Thread.sleep(1000);
				//Hit the "Add new client" button
				driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[3]/div[1]/div[2]/button")).click();
				Thread.sleep(3000);		
				//We will be redirected to add client form where we only have to add a unique client name
				for(char alphabet = 'a'; alphabet <='d'; alphabet++ )
				    {
						//Each time the value is set to blank
						String output = "";
				        output += alphabet;
				        //System.out.println(output);
				        //As client name should be unique, create a unique name for your client
				        driver.findElement(By.id("client_name")).sendKeys("CRM client Janet "+output+output);
				        //save client name
				        driver.findElement(By.xpath("//button[contains(text(),'Save Client')]")).click();
						//open the menu to go back to the client list
						driver.findElement(By.linkText("CLIENTS")).click();
						//hit all clients hit all client under the menu link
						driver.findElement(By.linkText("All Clients")).click();
						Thread.sleep(2000);
						//Again add a new client
						driver.findElement(By.xpath("//button[contains(text(),'+ Add New Client')]")).click();
						
				    }		
				driver.quit();
	}

}
